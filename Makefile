PROJ_NAME=main

CC=arm-none-eabi-gcc
AR=arm-none-eabi-ar
OBJCOPY=arm-none-eabi-objcopy
OBJDUMP=arm-none-eabi-objdump
SIZE=arm-none-eabi-size

CFLAGS += -DSTM32F051x8
CFLAGS += -mfloat-abi=soft
CFLAGS += -Wall -std=c99 -Os -fdata-sections -ffunction-sections
CFLAGS += -mlittle-endian -mcpu=cortex-m0  -march=armv6-m -mthumb

CFLAGS+=-IDrivers/CMSIS/Include
CFLAGS+=-IDrivers/CMSIS/Device/ST/STM32F0xx/Include
CFLAGS+=-IDrivers/STM32F0xx_HAL_Driver/Inc
CFLAGS+=-IInc
CFLAGS += -includestm32f0xx_hal_conf.h

LDFLAGS += -L. -Wl,--gc-sections -specs=nano.specs
LDLIBS += -lhal

LDSCRIPT_INC="SW4STM32/test Configuration"

HALSRC = $(wildcard Drivers/STM32F0xx_HAL_Driver/Src/*.c)
HALOBJ = $(HALSRC:.c=.o)

SRCS += Src/startup_stm32f051x8.s
SRCS += Drivers/CMSIS/Device/ST/STM32F0xx/Source/Templates/system_stm32f0xx.c
SRCS += $(wildcard Src/*.c)

$(PROJ_NAME).elf: libhal.a $(SRCS)
	$(CC) --specs=nosys.specs $(CFLAGS) $(SRCS) $(LDFLAGS) $(LDLIBS) -o $@ -L$(LDSCRIPT_INC) -TSTM32F051R8Tx_FLASH.ld
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf $(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin
	$(OBJDUMP) -St $(PROJ_NAME).elf >$(PROJ_NAME).lst
	$(SIZE) $(PROJ_NAME).elf


libhal.a: $(HALOBJ)
	$(AR) -r $@ $(HALOBJ)

clean:
	rm -f $(HALOBJ) libhal.a $(PROJ_NAME).elf $(PROJ_NAME).bin $(PROJ_NAME).hex $(PROJ_NAME).lst
