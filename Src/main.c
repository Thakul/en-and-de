/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/////// edited by user //////
#include<stdio.h>
//include<string.h>
#include<stdlib.h>
/////////////////////////////

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
int step = 5;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);
void encryption(int key, uint8_t * sendbuf, uint8_t * recvbuf);
void decryption(int key, uint8_t * sendbuf, uint8_t * recvbuf);

/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint8_t recvbuf[101];
  uint8_t sendbuf[200];
  snprintf((char *) sendbuf, 32, "\rEcho Device Initialized!\r\n");
  HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);

  while (1)
  {
    ///////// Input key ////////////////////////////
    uint8_t breakFromLoop = 0;
    int key = 0;
    HAL_UART_Transmit(&huart1, "Enter your key: ", 16, 10);
    for (uint8_t i = 0; i < 15; i++){
        uint8_t *p = recvbuf + i;
        if (HAL_UART_Receive(&huart1, p, 1, 10) == HAL_TIMEOUT){
            i--;
        } else if (*p == '\r') {
            *p = '\0';
            HAL_UART_Transmit(&huart1, "\r\n", 2, 10); //---edited by user---
            breakFromLoop = 1;
            break;
        } else {
            *(p+1) = '\0';
            snprintf((char *) sendbuf, 64, "%c", recvbuf[i]);  //---edited by user---
            HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
          }
    }

    if (breakFromLoop == 0) {
        recvbuf[15] = '\0';
    }else{
        sscanf((char *) recvbuf, "%d", &key);
    }
    /////////////////////////////////////////////////////////////////

      //HAL_UART_Transmit(&huart1, "Ready: ", 7, 10);
      HAL_UART_Transmit(&huart1, "Enter your massage: ", 20, 100);
      for (uint8_t i = 0; i < 100; i++){
          uint8_t *p = recvbuf + i;
          if (HAL_UART_Receive(&huart1, p, 1, 10) == HAL_TIMEOUT){
              i--;
          } else if (*p == '\r') {
              *p = '\0';
              HAL_UART_Transmit(&huart1, "\r\n", 2, 10);  //---edited by user---
              break;
          } else {
              *(p+1) = '\0';
              snprintf((char *) sendbuf, 64, "%c", recvbuf[i]);  //---edited by user---
              HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
          }
      }
      //recvbuf[15] = '\0';
      recvbuf[100] = '\0';

      encryption(key, sendbuf, recvbuf);
      //decryption(key, sendbuf, recvbuf);

      //snprintf((char *) sendbuf, 64, "Done\r\n");
      //HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);

      //snprintf((char *) sendbuf, 64, "Echo %s\r\n", recvbuf);
      //HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);

  /* USER CODE END WHILE */
/////////////////////////////
  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

////// function edited by user //////

void encryption(int key, uint8_t * sendbuf, uint8_t * recvbuf)
{
    HAL_UART_Transmit(&huart1, "Encrypting...\r\n", 15, 10);
    for(uint8_t i=0;i<=strlen((char *) recvbuf);i++)
    {
  		if(isupper((char)recvbuf[i]))			// check character is uppercase or not
  		{
  			if((char)recvbuf[i]+(key%26) > 90)	// in case character+key is larger than Z (ASCII = 90)
  			{
          sendbuf[i] = (((char)recvbuf[i]-90)+64)+(key%26);
  			}
  			else
  			{
          sendbuf[i] = (char)recvbuf[i]+(key%26);
  			}
  		}
  		else if(islower(recvbuf[i]))		// check character is lowercase or not
  		{
  			if((char)recvbuf[i]+(key%26) > 122)	// in case character+key is larger than z (ASCII = 122)
  			{
          sendbuf[i] = (((char)recvbuf[i]-122)+96)+(key%26);
  			}
  			else
  			{
          sendbuf[i] = (char)recvbuf[i]+(key%26);
  			}
  		}
  		else				// in case of other character except A-Z and a-z (symbol,space,enter,..)
  		{
        sendbuf[i] = (char)recvbuf[i];
  		}
    }

    snprintf((char *) sendbuf, 200, "%s\r\n", sendbuf);
    HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    HAL_UART_Transmit(&huart1, "Finished Encryption...\r\n", 24, 10);
}

void decryption(int key, uint8_t * sendbuf, uint8_t * recvbuf)
{
    HAL_UART_Transmit(&huart1, "Decrypting...\r\n", 15, 10);
    for(uint8_t i=0;i<=strlen((char *) recvbuf);i++)
    {
  		if(isupper((char)recvbuf[i]))			// check character is uppercase or not
  		{
  			if((char)recvbuf[i]-(key%26) < 65)	// in case character+key is larger than A (ASCII = 65)
  			{
          sendbuf[i] = (((char)recvbuf[i]-64)+90)-(key%26);
  			}
  			else
  			{
          sendbuf[i] = (char)recvbuf[i]-(key%26);
  			}
  		}
  		else if(islower(recvbuf[i]))		// check character is lowercase or not
  		{
  			if((char)recvbuf[i]-(key%26) < 97)	// in case character+key is larger than a (ASCII = 97)
  			{
          sendbuf[i] = (((char)recvbuf[i]-96)+122)-(key%26);
  			}
  			else
  			{
          sendbuf[i] = (char)recvbuf[i]-(key%26);
  			}
  		}
  		else				// in case of other character except A-Z and a-z (symbol,space,enter,..)
  		{
        sendbuf[i] = (char)recvbuf[i];
  		}
    }

    snprintf((char *) sendbuf, 200, "%s\r\n", sendbuf);
    HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    HAL_UART_Transmit(&huart1, "Finished Decryption...\r\n", 24, 10);
}


/////////////////////////////////////

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */

/**
  * @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
